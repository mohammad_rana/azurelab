#!/bin/bash

az group deployment create \
    --name lilydeploy \
    --resource-group AL_Academy \
    --template-file vm.json
